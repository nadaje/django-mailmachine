try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

CLASSIFIERS = [
    'Development Status :: 5 - Production/Stable',
    'Environment :: Console',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 2.7',
]

REQUIREMENTS = [
    'cssselect',
    'pyyaml',
    'premailer',
    'simplejson',
    'mailmachine @ git+git://github.com/paluh/mailmachine.git@1.2.4#egg=mailmachine-1.2.4',
]
setup(
    name='django-mailmachine',
    author='Tomasz Rybarczyk',
    author_email='paluho@gmail.com',
    classifiers=CLASSIFIERS,
    description='Mailmachine binding for django',
    install_requires=REQUIREMENTS,
    url='https://bitbucket.org/nadaje/django-mailmachine',
    packages=['django_mailmachine'],
    zip_safe=False,
    version = '0.1.1',
)
