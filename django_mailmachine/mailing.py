#-*- coding: utf-8 -*-
import urlparse
from django.conf import settings
from django.utils.safestring import mark_safe
from . import premailer
from mailmachine import enqueue, MailQueue

import socket


def get_mail_queue():
    redis_host = getattr(settings, 'MAIL_REDIS_HOST', 'localhost')
    redis_port = getattr(settings, 'MAIL_REDIS_PORT', 6379)
    redis_password = getattr(settings, 'MAIL_REDIS_PASSWORD', None)
    mail_queue = MailQueue(getattr(settings, 'MAIL_QUEUE', 'mails'), host=redis_host,
                           port=redis_port, password=redis_password)
    return mail_queue


def prepare_message(template_renderer, label, recipients, context=None,
                    send_as=None, reply_to=None, attachments=None):

    mail_templates_root = getattr(settings, 'MAILS_TEMPLATES_ROOT', 'mails')
    get_template = lambda t: '%s/%s/%s' % (mail_templates_root, label, t)

    subject = template_renderer(get_template('subject.txt'), context,
                                template_format='txt')

    body_txt = template_renderer(get_template('body.txt'), context,
                                template_format='txt')

    body_html = template_renderer(get_template('body.html'), context,
                                  template_format='html')
    subject = ''.join(subject.splitlines())
    base_url = urlparse.urlunparse(('http', getattr(settings, 'DOMAIN', socket.getfqdn()),
                                    '', '', '', ''))
    pm = premailer.Premailer(body_html, base_url=base_url)
    body_html = mark_safe(pm.transform())
    sender = reply_to or settings.DEFAULT_FROM_EMAIL
    message = {
        'subject': subject,
        'body': body_txt,
        'from_email': sender,
        'recipients': recipients,
        'alternatives': [(body_html, 'text/html')],
        'attachments': attachments,
    }
    return message


def send_mail(msg):
    mail_queue = get_mail_queue()
    enqueue(mail_queue=mail_queue, **msg)


def prepare_and_send(*args, **kwargs):
    message = prepare_message(*args, **kwargs)
    send_mail(message)


__all__ = ['prepare_and_send']
