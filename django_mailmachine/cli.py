import argparse
import simplejson

from . import prepare_and_send


def construct_parser(template_renderer):
    parser = argparse.ArgumentParser('django-mailmachine-sender')

    parser.add_argument('-l', '--label', action='store', required=True)
    parser.add_argument('-f', '--from-email', action='store', required=True)
    parser.add_argument('-r', '--recipient', action='append', dest='recipients',
                        required=True, metavar='RECIPIENT', help='you can use this option multiple times')

    parser.set_defaults(func=lambda args: prepare_and_send(template_renderer, label=args.label,
                                                           recipients=args.recipients,
                                                           context=(simplejson.loads(args.context) if args.context
                                                                                                   else None),
                                                           send_as=args.from_email))
    return parser


def dispatch(parser):
    args = parser.parse_args()
    args.func(args)
