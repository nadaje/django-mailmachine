from django.template.loader import render_to_string

from . import prepare_and_send as pas


def prepare_and_send(label, recipients, context=None,
                     send_as=None, reply_to=None, attachments=None):
    template_renderer = lambda template, context, template_format: render_to_string(template, context)
    return pas(template_renderer, label=label, recipients=recipients,
               context=context, send_as=send_as, reply_to=reply_to,
               attachments=attachments)
