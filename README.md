django-mailmachine
==================

Simple wrapper which provides mailmachine email helpers for django.


# Usage

# Basic Configuration

Configuration is really simple - you can overwrite two settings:

    :::python
    settings.MAILS_QUEUE # default='mails'

    settings.MAILS_TEMPLATES_ROOT # default='mails'

# Redis Configuration

If your redis configuration is not standard (localhost, 6379, passwordless), you have to point out
redis configuration which is used by mailing workers:

    :::python
    settings.MAIL_REDIS_HOST # default='localhost'

    settings.MAIL_REDIS_PORT # default=6379

    settings.MAIL_REDIS_PASSWORD # default=None


# Sending

Library uses two additional standard django settings:

    :::python
    settings.DOMAIN

    settings.DEFAULT_FROM_EMAIL


This library provides three simple functions (third one is just wrapper around first two):

    :::python
    def prepare_message(template_renderer, label, emails, extra_context=None,
                        send_as=None, reply_to=None, attachments=None):
        ...


    def send_mail(msg):
        ...


    def prepare_and_send(template_renderer, label, emails, extra_context=None,
                         send_as=None, reply_to=None, attachments=None):
        ...


`template_renderer` should be callable which implement following API:


    :::python
    renderer(template_path, context, template_format)


Where `template_path` will be constructed according to `settings.MAILS_TEMPLATES_ROOT` and provided label.
`template_format` will be one of `'html'` or `'txt'`. It is expected that for every email label there are
three templates available:

    :::python
    template_renderer('%s/%s/subject.txt' % (settings.MAILS_TEMPLATES_ROOT, label), context, template_format='txt')
    template_renderer('%s/%s/body.txt' % (settings.MAILS_TEMPLATES_ROOT, label), context, template_format='txt')
    template_renderer('%s/%s/body.html' % (settings.MAILS_TEMPLATES_ROOT, label), context, template_format='html')


# Logging
